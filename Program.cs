﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace HabboProductionChecker
{
    class Program
    {
        static void Main(string[] args)
        {
            WebClient VariablesDownloader = new WebClient();
            Console.WriteLine("Choose the hotel you want to find the PRODUCTION (ex: 'fr' for French Hotel, 'com' for US Hotel): ");
            string Hotel = Console.ReadLine();

            try
            {
                VariablesDownloader.Encoding = System.Text.Encoding.UTF8;
                VariablesDownloader.Headers.Add("User-Agent", "Mozilla/5.0 (Linux; U; Android 4.0.3; ko-kr; LG-L160L Build/IML74K) AppleWebkit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30");

                VariablesDownloader.DownloadFile(new Uri("http://www.habbo." + Hotel + "/gamedata/external_variables/ea494cd29b3693af2eef4939a606bd4cba30c24b"), @"external_variables_" + Hotel + ".txt");
                StreamReader reader = File.OpenText(@"external_variables_" + Hotel + ".txt");

                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    string[] items = line.Split('\t');
                    foreach (string Prod in items)
                    {
                        if (Prod.StartsWith("flash.client.url="))
                        {
                            Console.WriteLine("");
                            Console.WriteLine("Current PRODUCTION on habbo." + Hotel + ": " + Prod.Substring(43).TrimEnd('/'));
                            Console.WriteLine("");
                        }
                    }
                }

                reader.Close();
                File.Delete(@"external_variables_" + Hotel + ".txt");

                Console.WriteLine("");
                Console.WriteLine("Push ENTER Key to exit.");
                Console.ReadLine();
            }
            catch (Exception e) { Console.WriteLine(e.ToString()); Console.ReadLine(); }
        }
    }
}
